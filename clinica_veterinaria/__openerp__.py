{
   'name' : "Clinica_veterinaria",
   'version' : "1.0",
   'description' : 'Adds nutrition information to products',
   'author' : "Sergio, Prsperita",
   'depends' : ['sale'],
   'data' : ['clinica_view.xml','securiry/ir.model.access.csv'],
   'installable' : True,
}
