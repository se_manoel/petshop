# Dietfacts application
# Dietfacts application
from openerp import models, fields, api

# Extend product.template model with calories

class clinica_veterinaria_product_template(models.Model):
    _inherit = 'product.template'

    
   
    lastupdated = fields.Date('Last Updated')
    
    
class clinica_veterinaria_res_users_animal(models.Model):
    _name = 'res.users.animal'
    name = fields.Char("Especie")
    exame_date = fields.Datetime("Exame Date")
    item_ids = fields.One2many('res.users.exame','ex_id')
    user_id = fields.Many2one('res.users','Responsavel')
    large = fields.Boolean("Large Animal")
    raca = fields.Text('Raca')
    
class clinica_veterinaria_res_users_exame(models.Model):
    _name = 'res.users.exame'
    ex_id = fields.Many2one('res.users.animal')
    item_id = fields.Many2one('product.template','Exame')
    valor = fields.Float('Valor')
    seg = fields.Text("Especialidade")
    convenio = fields.Char("Convenio")

class clinica_veterinaria_product_nutrient(models.Model):
    _name = 'product.nutrient'
    name = fields.Char("Nutrient Name")
    uom_id = fields.Many2one('product.uom','Unit of Measure')
    description = fields.Text("Description")
    
class clinica_veterinaria_product_template_nutrient(models.Model):
    _name = 'product.template.nutrient'
    nutrient_id = fields.Many2one('product.nutrient',string="Product Nutrient")
    product_id = fields.Many2one('product.template')
    uom = fields.Char(related='nutrient_id.uom_id.name', string = "UOM", readyonly=True)
    value = fields.Float('Nutrient Value')
    dailypercent = fields.Float("Daily Recommended Value")
    
    
  